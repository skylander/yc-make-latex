Author: yanchuan <yanchuan@yanchuan.sg>

usage: ycmklatex [-h] [-f] [-c] [-v] [-V] [file [file ...]]


Compiles LaTeX/related file given on the command line. A sequence of files
maybe given and it will be compiled in order. Currently supports .tex, .plt


positional arguments:
  file           List of files to compile. They can be .tex, .plt.


optional arguments:
  -h, --help     show this help message and exit
  -f, --force    Force compile all the files given on the command line without
                 checking to see if it has been modified since last
                 compilation.
  -c, --clean    Clean up all intermediate files regardless of success or
                 failure.
  -v, --verbose  Print info messages.
  -V, --version  Print version information.


Version 0.1 (Jan 27, 2011)

- Supports compiling LaTeX files and running BibTeX alternatively until there are no citation errors (naive implementation).

- Supports running .plt scripts with gnuplot and detecting the eps files that are being produced.

- Supports converting .eps files to .pdf.

- Only compile files for which the output is missing or has changed.
